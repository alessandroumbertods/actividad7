/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Util.seed.Cola;
import Util.seed.Pila;
import java.util.Scanner;

/**
 *
 * @author Alessandro
 */
public class TestBalanceoSignos {

    public static void main(String[] args) {
        System.out.println("Validando [{()}] ");
        /**
         * Ejemplo: L={aa} => --> V L={[aa]} => --> F L=(a)[a]{b}--> V
         */
        Scanner t = new Scanner(System.in);
        String s = t.nextLine();
        Pila<Character> p = new Pila();
        Cola<Character> orden = new Cola();
        boolean flag = false;
        boolean malBalanceado = false;
        
        //Crear un arreglo de caracteres para comparar por el orden
        char arr[] = {'(', '{', '['};
        
        for (int i = 0; i < s.length() && !malBalanceado; i++) {
            char caracter = s.charAt(i);
            if (caracter == '(' || caracter == '[' || caracter == '{') {
                p.push(caracter);
            } else {
                //Para verificar que no sean letras
                if (caracter < 97 || caracter > 123) {
                    char simboloContrario = 0;
                    if (caracter == ')') {
                        simboloContrario = '(';
                    }
                    if (caracter == ']') {
                        simboloContrario = '[';
                    }
                    if (caracter == '}') {
                        simboloContrario = '{';
                    }
                    //Saco de la pila lo que cierra
                    if (p.top() == simboloContrario) {
                        orden.enColar(p.pop());
                    } else {
                        malBalanceado = true;
                    }
                    if (p.isVacia() && i + 1 != s.length()) {
                        flag = true;
                    }
                }
            }
        }
        //Reviso que el orden sea el correcto
        if (p.isVacia() && !malBalanceado) {
            if (flag) {
                System.out.println("Esta Correctamente Agrupado");
            } else {
                String ordenado = "";
                //Saco de la cola para ingresarlo a una cadena
                while (!orden.isVacia()) {
                    char simbolo = orden.deColar();
                    for (int i = 0; i < 3; i++) {
                        if (arr[i] == simbolo) {
                            ordenado += simbolo;
                        }
                    }
                }
                //Pregunto si la cadena ordenado es alguna de las combinaciones
                if (ordenado.equals("({[") || ordenado.equals("([") || ordenado.equals("({") ||
                    ordenado.equals("{[") || ordenado.equals("(") || ordenado.equals("{") || ordenado.equals("[")) {
                    System.out.println("Esta Correctamente Agrupado");
                } else {
                    System.out.println("No estan agrupados correctamente");
                }
            }
        } else {
            System.out.println("No es balanceado");
        }
    }
}
