/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import java.util.Random;
import Util.seed.Conjunto;
import java.util.Iterator;

/**
 *
 * @author Alessandro
 */
public class testConjunto {

    public static void main(String[] args) {
        
        System.out.println("**********Metodos de Conjuntos**********" + "\n");
        System.out.println("Metodo getUnion");
        Conjunto<Integer> c2 = crearConjunto(10);
        Conjunto<Integer> c1 = crearConjunto(10);
        System.out.println("Conjunto 1 " + c2.toString());
        System.out.println("Conjunto 2 " + c1.toString());
        System.out.println("Union de Conjuntos: {" + imprimir(c1.getUnion(c2)) + "}" + "\n");
        
        System.out.println("Metodo getInterseccion");
        c2 = crearConjunto(15);
        c1 = crearConjunto(15);
        System.out.println("Conjunto 1 " + c1.toString());
        System.out.println("Conjunto2 " + c2.toString());
        System.out.println("Interseccion de Conjuntos: {" + imprimir(c1.getInterseccion(c2)) + "}" + "\n");
       
        System.out.println("Metodo getDiferencia");
        c2 = crearConjunto(5);
        c1 = crearConjunto(5);
        System.out.println("Conjunto 1 " + c1.toString());
        System.out.println("Conjunto 2 " + c2.toString());
        System.out.println("Diferencia de Conjuntos: {" + imprimir(c1.getDiferencia(c2)) + "}" + "\n");
       
        System.out.println("Metodo getDiferenciaAntisimetrica");
        c2 = crearConjunto(5);
        c1 = crearConjunto(5);
        System.out.println("Conjunto 1 " + c1.toString());
        System.out.println("Conjunto 2 " + c2.toString());
        System.out.println("Diferencia Antisimetrica de Conjuntos: {" + imprimir(c1.getDiferenciaAsimetrica(c2)) + "}" + "\n");
        
        Conjunto<Conjunto<Object>> potencia = crearConjunto();
        System.out.println("Metodo getPotencia");
        System.out.println("Conjunto " + potencia.toString());
        System.out.print("Potencia de Conjuntos: {" + imprimirPotencia(potencia.get(0).getPotencia()) + "}" + "\n");
    }
    /**
     * Metodo que crea un conjunto de numeros del 1 al 20 de forma randomica
     * @param n no excede de 20, para facilidad al probar los metodos
     * @return Conjunto de enteros
     */
    private static Conjunto<Integer> crearConjunto(int n) {
        if (n <= 0) {
            throw new RuntimeException("No puedo crear conjunto");
        }
        Conjunto<Integer> c = new Conjunto();
        int noRepetidos[] = new int[20];
        while (n > 0) {
            int random = new Random().nextInt(20);
            while (noRepetidos[random] == random) {
                random = new Random().nextInt(20);
            }
            c.insertar(random);
            noRepetidos[random] = random;
            n--;
        }
        return c;
    }

    /**
     * Metodo que crea un conjunto de conjuntos, en este caso solo le damos el conjunto {1,2,3}
     * @return Conjunto de conjuntos
     */
    private static  Conjunto<Conjunto<Object>> crearConjunto() {
        Conjunto<Conjunto<Object>> c = new Conjunto();
        Conjunto<Object> sub = new Conjunto();
        sub.insertar(3);
        sub.insertar(2);
        sub.insertar(1);
        c.insertar(sub);
        return c;
    }

    /**
     * Metodo que imprime un Conjunto de enteros
     * @param c
     * @return String con los conjuntos
     */
    public static String imprimir(Conjunto<Integer> c) {
        String cadena = "";
        for (Integer x : c) {
            cadena += x + " ";
        }
        return cadena;
    }
    
    /**
     * Metodo que imprime la potencia de conjuntos
     * @param c
     * @return String con todos los conjuntos
     */
    public static String imprimirPotencia(Conjunto<Conjunto<Object>> c) {
        String cadena = "";
        for (Conjunto x : c) {
            cadena += x + " ";
        }
        return cadena;
    }
    
}
