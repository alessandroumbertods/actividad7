/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package Util.seed;

/**
 *      SÓLO SE PUEDEN USAR ITERADORES Y EN GETPARES UNICAMENTE USAR GET(..)
 * @author docente
 */
public interface IConjunto<T> {
    
    //{1, 2, 3} --> conjunto par: (1, 2), (1, 3), (2, 1), (2, 3), (3, 1), (3, 2)
    //{2, 3, 4} --> conjunto par: {2},{3},{4},{2,3},{3,4},{2,4},{2,3,4},{*}
    public T get(int i);
    public void set(int i, T info);
    public void insertar(T info);
    public Conjunto<T> getUnion(Conjunto<T> c1);
    public Conjunto<T> getInterseccion(Conjunto<T> c1);
    public Conjunto<T> getDiferencia(Conjunto<T> c1);
    public Conjunto<T> getDiferenciaAsimetrica(Conjunto<T> c1);
    public Conjunto<Conjunto<T>> getPotencia(); // es factorial !!!!!
}
