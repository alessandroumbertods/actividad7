/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Util.seed;

import java.util.Iterator;

/**
 * Wrapper (Envoltorio)
 *
 * @author docente
 */
public class Conjunto<T> implements IConjunto<T>, Iterable<T> {

    private ListaS<T> elementos = new ListaS();

    public Conjunto() {
    }

    /**
     * Metodo que da la union de conjuntos, unión de conjuntos A y B sin repetidos
     * @param c1
     * @return
     */
    @Override
    public Conjunto<T> getUnion(Conjunto<T> c1) {
        Conjunto<T> union = new Conjunto();
        ListaS<T> lista = new ListaS();
        Iterator<T> it = this.elementos.iterator();
        while (it.hasNext()) {
            T info = it.next();
            if (!containTo(c1, info)) {
                lista.insertarFin(info);
            }
        }
        lista.addAll(c1.elementos);
        union.elementos.addAll(lista);
        return union;
    }

    /**
     * Metodo que da la interseccion de los conjuntos
     *Los elementos que se repiten en los dos conjuntos
     * @param c1
     * @return
     */
    @Override
    public Conjunto<T> getInterseccion(Conjunto<T> c1) {
        Conjunto<T> interseccion = new Conjunto();
        ListaS<T> lista = new ListaS();
        Iterator<T> it = this.elementos.iterator();
        while (it.hasNext()) {
            T info = it.next();
            if (containTo(c1, info)) {
                lista.insertarFin(info);
            }
        }
        interseccion.elementos.addAll(lista);
        return interseccion;
    }

    /**
     * Metodo que da la diferencia de los conjuntos, es decir, los elementos del conjunto A 
     * que no estan en el conjunto B
     *A{1,2,3,4,5} B{1,3,5,7} = {2,4}
     * @param c1
     * @return
     */
    @Override
    public Conjunto<T> getDiferencia(Conjunto<T> c1) {
        Conjunto<T> diferencia = new Conjunto();
        ListaS<T> lista = new ListaS();
        Iterator<T> it = this.elementos.iterator();
        while (it.hasNext()) {
            T info = it.next();
            if (!containTo(c1, info)) {
                lista.insertarFin(info);
            }
        }
        diferencia.elementos.addAll(lista);
        return diferencia;
    }

    /**
     * Metodo que devuelve la diferencia asimetrica de los conjuntos
     * Ejemplo : A{1,2,3}  B{3,4,5} = {1,2,4,5}
     * @param c1
     * @return
     */
    @Override
    public Conjunto<T> getDiferenciaAsimetrica(Conjunto<T> c1) {
        Conjunto<T> diferencia = new Conjunto();
        ListaS<T> lista = new ListaS();
        Iterator<T> it = this.elementos.iterator();
        Iterator<T> it2= c1.elementos.iterator();
        
        while (it.hasNext()) {
            T info = it.next();
            if (!containTo(c1, info)) {
                lista.insertarFin(info);
            }
        }
        while (it2.hasNext()) {
            T info = it2.next();
            if (!containTo(this, info)) {
                lista.insertarFin(info);
            }
        }
        diferencia.elementos.addAll(lista);
        return diferencia;
    }

    /**
     * Metodo que devuelve la potencia del conjunto, metodo factorial
     * {2, 3, 4} --> conjunto par: {2},{3},{4},{2,3},{3,4},{2,4},{2,3,4},{*}
     * @return
     */
    @Override
    public Conjunto<Conjunto<T>> getPotencia() {
        Conjunto<Conjunto<T>> potencia = new Conjunto();
        ListaS<T> l = new ListaS();
        l.insertarInicio((T) "");

        for (int i = 0; i < this.elementos.getSize(); i++) {
            ListaS<T> listaTemporal = new ListaS();
            for (int j = 0; j < l.getSize(); j++) {
                Conjunto<T> factorial = new Conjunto();
                T elemento = (T) (l.get(j).toString() + this.elementos.get(i));
                factorial.elementos.insertarInicio(elemento);
                listaTemporal.insertarInicio(elemento);
                potencia.insertar(factorial);
            }
            l.addAll(listaTemporal);
        }
        return ordenarPotencia(potencia);
    }

    @Override
    public T get(int i) {
        return this.elementos.get(i);
    }

    @Override
    public void set(int i, T info) {
    }

    @Override
    public void insertar(T info) {
        this.elementos.insertarInicio(info);
    }

    @Override
    public String toString() {
        //return "Conjunto{" + elementos + '}';
        return "{" + elementos + "}";
    }

  public Iterator<T> iterator() {
        return new IteratorConjunto(this.elementos.getPos(0));
    }

    /**
     * Metodo que busca si el elemento está en un conjunto
     * @param c1
     * @param info
     * @return boolean si lo encuentra o no
     */
    public boolean containTo(Conjunto<T> c1, T info) {
        for (T info1 : c1.elementos) {
            if (info1.equals(info)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Metodo que ordena la potencia para que la imprima descendentemente
     * {2},{3},{4},{2,3},{3,4},{2,4},{2,3,4},{*}
     * @param c
     * @return 
     */
    public Conjunto<Conjunto<T>> ordenarPotencia(Conjunto<Conjunto<T>> c) {
        ListaS<T> lista = new ListaS();
        for (int i = 0; i < c.elementos.getSize(); i++) {
            Integer number = Integer.valueOf(c.elementos.get(i).get(0).toString());
            lista.insertarInicio((T) number);
        }
        
        //Ordeno la lista por el numero más grande, es una ventaja por no estar separada por comas
        lista.sort();

        Conjunto<Conjunto<T>> ordenado = new Conjunto();

        for (int i = 0; i < lista.getSize(); i++) {
            Conjunto<T> aux = new Conjunto();
            aux.elementos.insertarInicio(lista.get(i));
            ordenado.elementos.insertarFin(aux);
        }
        
        //Para agregar el par vacio
        Conjunto<T> vacio = new Conjunto();
        vacio.insertar((T) "");
        ordenado.elementos.insertarFin(vacio);

        return ordenado;
    }

}
