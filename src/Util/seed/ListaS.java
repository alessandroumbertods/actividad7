/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util.seed;

import java.util.Iterator;
import java.util.Objects;

/**
 * https://pythontutor.com/visualize.html#code=%20class%20ListaS%3CT%3E%7B%0A%20%20%0A%20%20%0A%20%20private%20Nodo%3CT%3E%20cabeza%3B%0A%20%20%20%20private%20int%20size%3B%0A%0A%20%20%20%20public%20ListaS%28%29%20%7B%0A%20%20%20%20%20%20this.cabeza%3Dnull%3B%0A%20%20%20%20%20%20this.size%3D0%3B%0A%20%20%20%20%7D%0A%0A%20%20%20%20public%20int%20getSize%28%29%20%7B%0A%20%20%20%20%20%20%20%20return%20size%3B%0A%20%20%20%20%7D%0A%0A%20%20%20%20public%20void%20insertarInicio%28T%20info%29%0A%20%20%20%20%7B%0A%20%20%20%20%20%20%20%20this.cabeza%3Dnew%20Nodo%28info,%20this.cabeza%29%3B%0A%20%20%20%20%20%20%20%20this.size%2B%2B%3B%0A%20%20%20%20%7D%0A%20%20%20%20%0A%20%20%20%20%0A%20%20%20%20public%20String%20toString%28%29%20%7B%0A%20%20%20%20%20%20%20%20if%20%28this.isVacia%28%29%29%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20return%20%22Lista%20Vac%C3%ADa%22%3B%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20String%20msg%20%3D%20%22%22%3B%0A%20%20%20%20%20%20%20%20for%20%28Nodo%3CT%3E%20i%20%3D%20this.cabeza%3B%20i%20!%3D%20null%3B%20i%20%3D%20i.getSig%28%29%29%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20msg%20%2B%3D%20i.toString%28%29%20%2B%20%22-%3E%22%3B%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20return%20msg%20%2B%20%22null%22%3B%0A%0A%20%20%20%20%7D%0A%0A%20%20%20%20public%20boolean%20isVacia%28%29%20%7B%0A%20%20%20%20%20%20%20%20return%20this.cabeza%20%3D%3D%20null%3B%0A%20%20%20%20%7D%0A%20%20%20%20%0A%20%20%20%20%0A%20%20%20%20%0A%20%20%20%20public%20void%20insertarFin%28T%20info%29%20%7B%0A%20%20%20%20%20%20%20%20if%20%28this.isVacia%28%29%29%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20this.insertarInicio%28info%29%3B%0A%20%20%20%20%20%20%20%20%7D%20else%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20Nodo%3CT%3E%20x%3DgetPos%28this.size%20-%201%29%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20x.setSig%28new%20Nodo%28info,%20null%29%29%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20this.size%2B%2B%3B%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%7D%0A%0A%20%20%20%20private%20Nodo%3CT%3E%20getPos%28int%20posicion%29%20%7B%0A%20%20%20%20%20%20%20%20this.validar%28posicion%29%3B%0A%20%20%20%20%20%20%20%20Nodo%3CT%3E%20x%20%3D%20this.cabeza%3B%0A%20%20%20%20%20%20%20%20//%20a%3D3,%20b%3Da%3B%20b%3D3%0A%20%20%20%20%20%20%20%20while%20%28posicion--%20%3E%200%29%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20%0A%20%20%20%20%20%20%20%20%20%20%20%20x%20%3D%20x.getSig%28%29%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20//pos%3Dpos-1%20%3A%20--%3E%20first%20class%20OP%20%3A%28%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20return%20x%3B%0A%20%20%20%20%7D%0A%0A%20%20%20%20private%20void%20validar%28int%20i%29%20%7B%0A%20%20%20%20%20%20%20%20if%20%28this.isVacia%28%29%20%7C%7C%20i%20%3C%200%20%7C%7C%20i%20%3E%3D%20this.size%29%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20throw%20new%20RuntimeException%28%22No%20es%20v%C3%A1lida%20la%20posici%C3%B3n%22%29%3B%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%7D%0A%20%20%20%20%0A%20%20%20%20%0A%20%20%20%20%0A%20%20%20%20%0A%20%20%7D%0A%0A%0A%20class%20Nodo%3CT%3E%7B%0A%20%20%0A%20%20private%20T%20info%3B%0A%20%20private%20Nodo%3CT%3E%20sig%3B%0A%20%20%0A%20%20%0A%20%20Nodo%28%29%20%7B%0A%20%20%20%20%7D%0A%0A%20%20%20%20Nodo%28T%20info,%20Nodo%3CT%3E%20sig%29%20%7B%0A%20%20%20%20%20%20%20%20this.info%20%3D%20info%3B%0A%20%20%20%20%20%20%20%20this.sig%20%3D%20sig%3B%0A%20%20%20%20%7D%0A%0A%20%20%20%20T%20getInfo%28%29%20%7B%0A%20%20%20%20%20%20%20%20return%20info%3B%0A%20%20%20%20%7D%0A%0A%20%20%20%20void%20setInfo%28T%20info%29%20%7B%0A%20%20%20%20%20%20%20%20this.info%20%3D%20info%3B%0A%20%20%20%20%7D%0A%0A%20%20%20%20Nodo%3CT%3E%20getSig%28%29%20%7B%0A%20%20%20%20%20%20%20%20return%20sig%3B%0A%20%20%20%20%7D%0A%0A%20%20%20%20void%20setSig%28Nodo%3CT%3E%20sig%29%20%7B%0A%20%20%20%20%20%20%20%20this.sig%20%3D%20sig%3B%0A%20%20%20%20%7D%0A%20%20%20%20%0A%20%20%20%20public%20String%20toString%28%29%20%7B%0A%20%20%20%20%20%20%20%20return%20info.toString%28%29%3B%0A%20%20%20%20%7D%0A%0A%20%20%7D%0A%0A%0A%0A%0A//Paquete%20vista%0Apublic%20class%20TestListaS%20%7B%0A%20%20%20%20public%20static%20void%20main%28String%5B%5D%20args%29%20%7B%0A%20%20%20%20%20%20%0A%20%20%20%20%20%20//Clase%20parametrizada%20%3A%20Integer%0A%20%20%20%20%20%20ListaS%3CInteger%3E%20lista%3Dnew%20ListaS%28%29%3B%0A%20%20%20%20%20%20lista.insertarInicio%284%29%3B%0A%20%20%20%20%20%20lista.insertarInicio%285%29%3B%0A%20%20%20%20%20%20lista.insertarInicio%286%29%3B%0A%20%20%20%20%20%20lista.insertarFin%287%29%3B%0A%20%20%20%20%20%20System.out.println%28lista.toString%28%29%29%3B%0A%0A%20%20%20%20%7D%0A%7D&cumulative=false&curInstr=101&heapPrimitives=nevernest&mode=display&origin=opt-frontend.js&py=java&rawInputLstJSON=%5B%5D&textReferences=false
 *
 * @author Estudiantes
 */
public class ListaS<T> implements Iterable<T> {

    private Nodo<T> cabeza;
    private int size;

    public ListaS() {
        this.cabeza = null;
        this.size = 0;
    }

    public int getSize() {
        return size;
    }

    public void insertarInicio(T info) {

        this.cabeza = new Nodo(info, this.cabeza);
        this.size++;
    }

    public void insertarFin(T info) {
        if (this.isVacia()) {
            this.insertarInicio(info);
        } else {
            getPos(this.size - 1).setSig(new Nodo(info, null));
            this.size++;
        }
    }

    public Nodo<T> getPos(int posicion) {
        this.validar(posicion);
        Nodo<T> x = this.cabeza;
        // a=3, b=a; b=3
        while (posicion-- > 0) {

            x = x.getSig();
            //pos=pos-1 : --> first class OP :(
        }
        return x;
    }

    private void validar(int i) {
        if (this.isVacia() || i < 0 || i >= this.size) {
            throw new RuntimeException("No es válida la posición");
        }
    }

    @Override
    public String toString() {
        if (this.isVacia()) {
            return "Lista Vacía";
        }
        String msg = this.cabeza.toString();
        for (Nodo<T> i = this.cabeza.getSig(); i != null; i = i.getSig()) {
            msg += ",";
            msg += i.toString();
        }
        return msg; //+ "null";

    }

    public boolean isVacia() {
        return this.cabeza == null;
    }

    public T get(int i) {
        try {
            this.validar(i);
            Nodo<T> pos = this.getPos(i);
            return pos.getInfo();
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return null;
        }
    }

    /**
     *
     * @param i
     * @param info
     */
    public void set(int i, T info) {
        try {
            this.validar(i);
            Nodo<T> pos = this.getPos(i);
            pos.setInfo(info);
        } catch (Exception e) {
            System.err.println(e.getMessage());

        }
    }

    public T remove(int i) {
        this.validar(i);
        Nodo<T> borrar = this.cabeza;
        if (i == 0) {
            this.cabeza = this.cabeza.getSig();

        } else {
            Nodo<T> anterior = this.getPos(i - 1);
            borrar = anterior.getSig();
            anterior.setSig(borrar.getSig());
        }

        this.size--;
        borrar.setSig(null);
        return borrar.getInfo();
    }

    public boolean addAll(ListaS<T> l2) {
        if (this.isVacia() && l2.isVacia()) {
            return false;
        }

        if (l2.isVacia()) {
            return false;
        }

        if (this.isVacia()) {
            this.cabeza = l2.cabeza;

        } else {
            Nodo<T> ultimo = this.getPos(size - 1);
            ultimo.setSig(l2.cabeza);

        }

        this.size += l2.size;
        l2.clear();
        return true;
    }

    public void clear() {
        this.cabeza = null;
        this.size = 0;
    }

    /**
     * Crear una subLista que me de una cadena desde un indice inicio hasta un
     * indice final
     *
     * @param fromIndex
     * @param toIndex
     * @return
     */
    public ListaS<T> subList(int fromIndex, int toIndex) {
        this.validar(fromIndex);
        this.validar(toIndex);

        if (this.isVacia()) {
            throw new RuntimeException("Lista Vacia");
        }

        Nodo<T> from = this.getPos(fromIndex);
        ListaS<T> subLista = new ListaS();
        int number = toIndex - fromIndex;
        for (Nodo<T> r = from; r != null; r = r.getSig()) {
            if (number == 0) {
                r.setSig(null);
                subLista.cabeza = from;
            }
            number--;
        }
        return subLista;
    }

    /**
     * Metodo que verifica si un nodo es un palindromo
     *
     * @return
     */
    public boolean isPalin() {
        if (this.isVacia()) {
            throw new RuntimeException("No se puede realizar el proceso");
        }

        ListaS<T> l2 = crearInvertida();
        return (this.equals(l2));

    }

    /**
     * Metodo que invierte una ListaS
     *
     * @return una nueva ListaS invertida
     */
    public ListaS<T> crearInvertida() {
        if (this.isVacia()) {
            throw new RuntimeException("No es válido operación");
        }
        ListaS<T> l2 = new ListaS();
        Nodo<T> ultimo_l2 = null;
        for (Nodo<T> actual = this.cabeza; actual != null; actual = actual.getSig()) {
            T info = actual.getInfo();
            Nodo<T> nuevo_l2 = new Nodo(info, ultimo_l2);
            ultimo_l2 = nuevo_l2;
        }
        l2.cabeza = ultimo_l2;
        l2.size = this.size;
        return l2;

    }

    /**
     * Metodo que invierte la Lista sin crear una nueva ListaS
     *
     */
    public void invertir() {
        if (this.isVacia()) {
            throw new RuntimeException("No es válido operación");
        }
        if (this.size == 1) {
            return;
        }

        Nodo<T> ult = null;
        Nodo<T> actual = this.cabeza;
        Nodo<T> sig;
        while (actual != null) {
            sig = actual.getSig();
            actual.setSig(ult);
            ult = actual;
            actual = sig;

        }

        this.cabeza = ult;

    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.cabeza);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ListaS<T> other = (ListaS<T>) obj;
        if (this.isVacia() || other.isVacia()) {
            return false;
        }
        if (this.size != other.size) {
            return false;
        }
        // T debe tener implementado equals
        for (Nodo<T> c = this.cabeza, c2 = other.cabeza; c != null; c = c.getSig(), c2 = c2.getSig()) {
            if (!c.getInfo().equals(c2.getInfo())) {
                return false;
            }
        }
        return true;
    }

    /**
     * Metodo que verifica si hay repetidos en una lista
     *
     * @return boolean si es o no es repetida
     */
    public boolean repetidos() {
        for (Nodo<T> x1 = this.cabeza; x1.getSig() != null; x1 = x1.getSig()) {
            for (Nodo<T> x2 = x1.getSig(); x2 != null; x2 = x2.getSig()) {
                if (x1.getInfo().equals(x2.getInfo())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Metodo que elimina los repetidos de la ListaS
     */
    public void eliminarRepetidos() {
        if (this.isVacia()) {
            throw new RuntimeException("Lista está Vacia");
        }
        Nodo<T> x1 = this.cabeza;
        Nodo<T> anterior = null;

        while (x1 != null) {
            Nodo<T> x2 = x1.getSig();
            anterior = x1;

            while (x2 != null) {
                if (x1.getInfo().equals(x2.getInfo())) {
                    anterior.setSig(x2.getSig());
                } else {
                    anterior = x2;
                }
                x2 = x2.getSig();
            }
            x1 = x1.getSig();
        }
    }

    /**
     * Metodo que retorna si la Lista2 es igual a esta ListaS
     *
     * @param l2
     * @return booleano si son o no son iguales
     */
    public boolean listaRepetida(ListaS<T> l2) {
        for (Nodo<T> x1 = this.cabeza, x2 = l2.cabeza; x1.getSig() != null; x1 = x1.getSig(), x2 = x2.getSig()) {
            if (!x1.getInfo().equals(x2.getInfo())) {
                return false;
            }
        }
        return true;
    }

    /**
     * Metodo que retorna una nueva lista a partir de dos listas, NO SE HA
     * CORREGIDO
     *
     * @param l1
     * @param l2
     * @return Una nueva ListaS
     */
    public ListaS<T> crearLista(ListaS<T> l1, ListaS<T> l2) {
        ListaS<T> listaUnida = new ListaS();
        listaUnida.cabeza = l1.cabeza;
        for (Nodo<T> x1 = listaUnida.cabeza; x1.getSig() != null; x1 = x1.getSig()) {
            if (x1.getSig().getSig() == null) {
                x1.setSig(l2.cabeza);
                break;
                //return listaUnida;
            }

        }
        return listaUnida;
    }

    /**
     * Metodo que crea una nueva ListaS y la ordena de forma ascendente
     *
     * @param l2
     * @return ListaS de forma ascendente uniendo las dos listas
     */
    public ListaS<T> ordenarAscendente(ListaS<T> l2) {
        ListaS<T> lista = new ListaS();
        this.sort();
        l2.sort();
        Nodo<T> a = this.cabeza;
        Nodo<T> b = l2.cabeza;

        int sizeA = 0;
        int sizeB = 0;

        while (a != null || b != null) {
            if (a.compareTo(b) > 0) {
                lista.insertarFin(a.getInfo());
                a = a.getSig();
                sizeA++;
            } else {
                lista.insertarFin(b.getInfo());
                b = b.getSig();
                sizeB++;
            }
        }

        if (sizeA < sizeB) {
            while (b != null) {
                lista.insertarFin(b.getInfo());
                b = b.getSig();
                sizeB++;
            }
        } else {
            if (sizeA > sizeB) {
                while (a != null) {
                    lista.insertarFin(a.getInfo());
                    a = a.getSig();
                    sizeA++;
                }
            }
        }
        return lista;
    }

    /**
     * Metodo de ordenamiento
     *
     * @param l
     * @return ListaS Ordenada
     */
    public void sort() {
        mergeSort(this.cabeza);
    }

    /**
     * Metodo mergeSort recursivo para ordenar la ListaS
     *
     * @param node
     * @return Nodo cabeza ordenado
     */
    private Nodo<T> mergeSort(Nodo<T> node) {
        if (node == null || node.getSig() == null) {
            return node;
        }

        // Dividir la lista en dos mitades
        Nodo<T> middle = getMiddle(node);
        Nodo<T> nextOfMiddle = middle.getSig();
        middle.setSig(null);

        // Ordenar recursivamente cada mitad llamando al método mergeSort
        Nodo<T> left = mergeSort(node);
        Nodo<T> right = mergeSort(nextOfMiddle);

        // Mezclar las dos mitades ordenadas para producir una lista ordenada completa
        return merge(left, right);
    }

    /**
     * Metodo que el Nodo Mitad de la Lista
     *
     * @param node
     * @return Nodo medio
     */
    private Nodo<T> getMiddle(Nodo<T> node) {
        if (node == null) {
            return node;
        }

        Nodo<T> slow = node;
        Nodo<T> fast = node;

        while (fast.getSig() != null && fast.getSig().getSig() != null) {
            slow = slow.getSig();
            fast = fast.getSig().getSig();
        }

        return slow;
    }

    /**
     * Metodo que mezcla las dos mitades ordenadas para producir una lista
     * ordenada completa
     *
     * @param left
     * @param right
     * @return ListaS ordenada completa
     */
    private Nodo<T> merge(Nodo<T> left, Nodo<T> right) {
        Nodo<T> result = null;

        if (left == null) {
            return right;
        }

        if (right == null) {
            return left;
        }

        if (left.compareTo(right) <= 0) {
            result = left;
            result.setSig(merge(left.getSig(), right));
        } else {
            result = right;
            result.setSig(merge(left, right.getSig()));
        }

        return result;
    }

    /**
     * Metodo que divide una cadena por un dato dado
     *
     * @param info
     * @return Una cadena de Listas
     */
    public ListaS<T>[] split(T info) {
        if (this.isVacia()) {
            throw new RuntimeException("Lista Vacia");
        }
        ListaS<T> separador[] = new ListaS[this.size];
        // Nodo<T> x = this.cabeza;
        int i = 0;

        ListaS<T> subLista = new ListaS();
        for (Nodo<T> x = this.cabeza; x != null; x = x.getSig()) {
            if (!x.getInfo().equals(info)) {
                subLista.insertarFin(x.getInfo());
            } else {

                separador[i] = subLista;
                subLista = new ListaS();
                i++;
            }
            if (!subLista.isVacia()) {
                separador[i] = subLista;
            }
        }

        return separador;
    }

    /**
     * Metodo que invierte una ListaS
     */
    public void invertirLista() {
        Nodo<T> ultimo = null;
        Nodo<T> aux;
        Nodo<T> x = this.cabeza;

        while (x.getSig() != null) {
            aux = x.getSig();//Paso el nodo al siguiente
            x.setSig(ultimo);//El nodo x lo conecto con el nodo de atras
            ultimo = x;//El nodo de atras lo paso hacia adelante
            x = aux;//el nodo x lo paso adelante donde esta el aux
        }
        this.cabeza = ultimo;
    }

    /**
     * Metodo que elimina el elemento más grande de una ListaS
     */
    public void removerElementoMasGrande() {
        Nodo<T> x = this.cabeza;
        Nodo<T> mayor = x;
        Nodo<T> anterior = x;
        while (x != null) {
            if (x.compareTo(x.getSig()) > 0) {
                if (mayor.compareTo(x) < 0) {
                    mayor = x;
                }

            }
            if (x.getSig() != null) {
                x = x.getSig();
            } else {
                break;
            }
        }
        x = this.cabeza;
        if (this.cabeza.equals(mayor)) {
            this.cabeza = this.cabeza.getSig();
        } else {
            while (x != null) {
                if (!x.equals(mayor)) {
                    anterior = x;
                } else {
                    anterior.setSig(x.getSig());
                }
                if (x.getSig() != null) {
                    x = x.getSig();
                } else {
                    break;
                }
            }
        }
    }

    public void datoBomba(T info) {
        Nodo<T> start = this.cabeza, backBomb = null, nextBomb = null, bomb = null, end = null;
        Nodo<T> x = this.cabeza;
        if (x.getInfo().equals(info)) {
            conectarPrimero(x);
            return;
        }
        while (x != null) {
            if (x.getSig() != null && x.getSig().getInfo().equals(info)) {
                if (x.getSig().getSig() == null) {
                    conectarUltimo(x.getSig());
                    return;
                }
                backBomb = x;
                bomb = x.getSig();
            }

            if (x.getSig() == null) {
                end = x;
            }

            x = x.getSig();
        }
        this.cabeza = bomb.getSig();
        bomb.setSig(start);
        end.setSig(bomb);
        backBomb.setSig(null);
    }

    public void conectarPrimero(Nodo<T> bomb) {
        this.cabeza = this.cabeza.getSig();
        Nodo<T> x = this.cabeza;
        while (x != null) {
            if (x.getSig() == null) {
                bomb.setSig(null);
                x.setSig(bomb);
                break;
            }
            x = x.getSig();
        }
    }

    public void conectarUltimo(Nodo<T> bomb) {
        Nodo<T> cuerpo = this.cabeza;
        Nodo<T> anterior = null;
        bomb.setSig(cuerpo);

        while (cuerpo != null) {
            if (cuerpo.equals(bomb)) {
                anterior.setSig(null);
            } else {
                anterior = cuerpo;
            }
            cuerpo = cuerpo.getSig();
        }

        this.cabeza = bomb;
    }

    public void setNodo(int index, T info) {
        int i = 0;
        for (Nodo<T> x = this.cabeza; x.getSig() != null; x = x.getSig()) {
            if (index == i) {
                x.setInfo(info);
                break;
            }
            i++;

        }
    }

    public void addAll(int index, ListaS<T> l) {
        int i = 0;
        Nodo<T> siguientes = null;
        for (Nodo<T> x = this.cabeza; x != null; x = x.getSig()) {
            if (index == i) {
                siguientes = x.getSig();
                x.setSig(l.cabeza);
            }
            i++;
            if (x.getSig() == null) {
                x.setSig(siguientes);
                break;
            }
        }
    }
       
   @Override
    public Iterator<T> iterator() {
        return new IteratorLS(this.cabeza);
    }

}
