/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Util.seed;

/**
 *
 * @author madarme
 */
public class Cola<T> {
    
    private ListaCD<T> lista = new ListaCD();

    public Cola() {
    }

    public void enColar(T info) {
        this.lista.insertarFin(info);
    }

    public T deColar() {
        if (this.lista.isVacia()) {
            throw new RuntimeException("Cola Vacía");
        }
        return this.lista.remove(0);
    }

    public boolean isVacia() {
        return this.lista.isVacia();
    }

    public int size() {
        return this.lista.getSize();
    }
}
